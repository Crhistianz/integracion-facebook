import json

from utils.sendRequests import SendRequests
from utils.logger import LogInfo


class FootBallController(SendRequests):

    def listCountries(self):
        countries = self.get('football', 'countries')
        countries = json.loads(countries)['api']['countries']

        # Extrayendo variables de entorno
        envFootBall = self.footBall()
        allowedCountries = envFootBall['FILTERS']['ALLOWED_COUNTRIES']
        dataCountries = []

        for country in countries:
            # Filtrar los países que están permitidos para mostrar
            if country['country'] in allowedCountries:
                 dataCountries.append(country)

        return dataCountries

    def listLeagues(self):
        pass

    def listTeams(self):
        pass

    def listPlayers(self):
        pass

    def listDetailPlater(self):
        pass

    def log(self, data):
        LogInfo('Clase FootBallController', data)

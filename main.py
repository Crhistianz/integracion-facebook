from flask import Flask
from utils.environment import Environment

def createAPP():
    app = Flask(__name__)

    # Routes de Facebook
    from routes.facebookRoutes import facebookBP
    app.register_blueprint(facebookBP, url_prefix='/facebook')

    # Routes de Twitter
    from routes.twitterRoutes import twitterBP
    app.register_blueprint(twitterBP, url_prefix='/twitter')

    # Routes de Football
    from routes.footballRoutes import footballBP
    app.register_blueprint(footballBP, url_prefix='/football')

    return app


if __name__ == '__main__':
    # Instanciar la clase Environment
    env = Environment()
    # Obtener la variables de entornos para el proyecto
    config = env.general()

    app = createAPP()
    app.run(port = config['PORT'], debug = config['DEBUG'])

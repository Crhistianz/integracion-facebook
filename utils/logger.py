import json


def LogInfo(message, data):
    print(message)
    print(json.dumps(data, indent=2))

def LogError(message, data):
    pass

def LogDebug(message, data):
    pass


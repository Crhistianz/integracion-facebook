import os

from dotenv import load_dotenv
load_dotenv()


class Environment():

    def general(self):
        return{
            'PORT': os.getenv("PORT", "8081"),
            'DEBUG': self.convertStringToBooleano(os.getenv('DEBUG', False))
        }

    def facebook(self):
        return {
            'TOKEN': os.getenv("TOKEN", "NONE"),
            'ACCESS_TOKEN': os.getenv("ACCESS_TOKEN", "NONE"),
            'URL_API': os.getenv("URL_API_GRAPH", "NONE")
        }

    def twilio(self):
        pass

    def footBall(self):
        return {
            'TOKEN': os.getenv("TOKEN", "NONE"),
            'URL_API': os.getenv("URL_API_FOOTBALL", "NONE"),
            'X-RAPIDAPI-HOST':os.getenv("X-RAPIDAPI-HOST", "NONE"),
            'X-RAPIDAPI-KEY': os.getenv("X-RAPIDAPI-KEY", "NONE"),
            'FILTERS': {
                'ALLOWED_COUNTRIES': self.convertStringToList(os.getenv("ALLOWED_COUNTRIES")),
            }
        }

    def convertStringToList(self, parameters):
        newParameters = []
        for parameter in parameters.split(','):
            newParameters.append(parameter.strip())

        return newParameters

    def convertStringToBooleano(self, parameter):
        if parameter.upper() == 'TRUE':
            return True

        return False